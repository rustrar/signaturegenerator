﻿using System;

namespace SignatureGenerator
{
    class Program    
    {

        static void Main(string[] args)
        {
            int blockSize = 0;
            Console.WriteLine("Введите полный путь к файлу:");
            string pathToFile = Console.ReadLine();

            Console.WriteLine("Введите размер блока в байтах:");
            while (!int.TryParse(Console.ReadLine(), out blockSize))            
                Console.WriteLine("Введённое число неверно, попробуйте ещё раз.");            

            WorkerThreadSheduiler workerThreadSheduiler = new WorkerThreadSheduiler();

            try
            { 
                workerThreadSheduiler.GenerateSignature(pathToFile, blockSize);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Во время выполнения программы произошла ошибка: {0}\nStack trace:\n{1}", ex.Message, ex.StackTrace);
            }            
            Console.ReadLine();            
        }

       
    }
}
