﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace SignatureGenerator
{

    class WorkerThread
    {
        public static long _blockCount;
        AutoResetEvent _waitForComplete;
        AutoResetEvent _waitForRun;
        Thread _workerTread;        
        byte[] _data;
        int _number;       
        bool _isComplete;   

        public WorkerThread(AutoResetEvent waitForHandleComplete, int i)
        {
            _waitForComplete = waitForHandleComplete;
            _waitForRun = new AutoResetEvent(false);
            _workerTread = new Thread(new ThreadStart(CalculateSignature));
            _workerTread.IsBackground = true;
            _workerTread.Name = i.ToString();          
            _workerTread.Start();
        }

        public void InitAndStart(byte[] buffer, int number)
        {
            _data = buffer;
            _number = number;
            _waitForRun.Set();
        }

        public void FinishWorkThread()
        {
            _isComplete = true;            
        }

        public void CalculateSignature()
        {
            try
            {
                using (SHA256 sha = SHA256.Create())
                {
                    while (!_isComplete)
                    {
                        _waitForRun.WaitOne();
                        byte[] hash = sha.ComputeHash(_data);
                        StringBuilder hashStringBuilder = new StringBuilder();                        
                        foreach (byte b in hash)
                            hashStringBuilder.Append(b.ToString("x2"));
                        Console.WriteLine(string.Format("Thread#{0} [{1}] из {2}: {3}", _workerTread.Name, _number, _blockCount, hashStringBuilder));
                        _waitForComplete.Set();
                    }                    
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Во время выполнения программы произошла ошибка: {0}\nStack trace:\n{1}", ex.Message, ex.StackTrace);
            }
        } 
    }
}


