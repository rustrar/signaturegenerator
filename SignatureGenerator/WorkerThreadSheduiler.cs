﻿using System;
using System.IO;
using System.Threading;

namespace SignatureGenerator
{
    class WorkerThreadSheduiler
    {
        public static int _processorCount = Environment.ProcessorCount;
        AutoResetEvent[] _waitForHandleComplete = new AutoResetEvent[_processorCount];

        public WorkerThreadSheduiler()
        {
            for (int i = 0; i < _processorCount; i++)           
                _waitForHandleComplete[i] = new AutoResetEvent(true);            
        }

        public void GenerateSignature(string pathToFile, int blockSize)
        {
            WorkerThread[] workerThread = new WorkerThread[_processorCount];
            for (int i = 0; i < _processorCount; i++)            
                workerThread[i] = new WorkerThread(_waitForHandleComplete[i], i);            

            using (FileStream file = new FileStream(pathToFile, FileMode.Open, FileAccess.Read))
            {
                long sizeOfFile = file.Length;
                long blockCount = (sizeOfFile / blockSize);
                WorkerThread._blockCount = blockCount;
                for (int i = 0; i < blockCount; i++)
                {
                    int workerThreadIndex = WaitHandle.WaitAny(_waitForHandleComplete);
                    byte[] buffer = new byte[blockSize];
                    file.Read(buffer, 0, blockSize);
                    workerThread[workerThreadIndex].InitAndStart(buffer, i);
                }
                for (int i = 0; i < workerThread.Length; i++)
                    workerThread[i].FinishWorkThread();
            }
                       
        }
    }
}